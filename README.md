# WITAJCIE KOCHANI 👋

## Zatem może ja opowiem co nie co o sobie:

:woman_teacher: Aktualnie jestem studentką AGHu.

:woman_student:	 Jestem na kierunku Informatyka i Systemy inteligentne na wydziale EAIiIB.

🔭 W przyszłości chciałabym bardzo pracować dla NASA.

:church:	Jestem chrześcijanką.

:running_woman:	Mają pasją są biegi długodystansowe.

:people_hugging: Mega mocno interesuję się ludźmi, kocham ich, ich różnorodne charaktery i cudowne pomysły.

:cat2:	Kocham z całęgo serduszka koty.

### Ogólnie można się ze mną kontakotować zawsze, kiedy tylko ktoś tego zapragnie. Jestem do Waszej dyspozycji zawsze, kiedy nie śpię lub się nie uczę. 

## Służę zawsze pomocą, uśmiechem i dobrym żartem. :white_heart:	

![Cytat na całę życie](https://dailyverses.net/images/pl/ubg/xl/efezjan-4-32-3.jpg)

